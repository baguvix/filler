/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hard_queue.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/14 15:30:56 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:46:34 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "hard_queue.h"

t_qd	*create_queue(size_t size)
{
	t_qd	*q;

	q = (t_qd *)ft_memalloc(sizeof(t_qd));
	if (q)
	{
		q->size = size;
		q->queue = (t_queue *)ft_memalloc(size * sizeof(t_queue));
		if (!q->queue)
		{
			free(q);
			q = NULL;
		}
	}
	return (q);
}

int		enqueue(t_qd *q, unsigned x, unsigned y, int w)
{
	if (q->queue[q->end].status != 0)
	{
		write(2, "PANIC!!! QUEUE FULLFILLED!!!\n", 28);
		return (1);
	}
	q->queue[q->end].dot.x = x;
	q->queue[q->end].dot.y = y;
	q->queue[q->end].dot.w = w;
	q->queue[q->end].status = 1;
	++q->end;
	if (q->end == q->size)
		q->end = 0;
	return (0);
}

int		dequeue(t_qd *q, unsigned *x, unsigned *y, int *w)
{
	if (q->end == q->curr)
		return (1);
	*x = q->queue[q->curr].dot.x;
	*y = q->queue[q->curr].dot.y;
	*w = q->queue[q->curr].dot.w;
	q->queue[q->curr].status = 0;
	++q->curr;
	if (q->curr == q->size)
		q->curr = 0;
	return (0);
}

void	*kill_queue(t_qd **q)
{
	free((*q)->queue);
	free(*q);
	*q = NULL;
	return (NULL);
}
