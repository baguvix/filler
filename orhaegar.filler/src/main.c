/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/09 17:18:46 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:10:22 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"
#include "ft_printf.h"

int		read_fields(t_game *g)
{
	ft_bzero(g->heat_map.data, g->heat_map.x * g->heat_map.y * sizeof(int));
	if (get_dimensions(&g->plateau.x, &g->plateau.y)
		|| get_field(&g->plateau, 'm'))
		return (1);
	if (get_dimensions(&g->piece.x, &g->piece.y)
		|| get_field(&g->piece, 'p'))
		return (1);
	return (0);
}

int		main(void)
{
	unsigned	x;
	unsigned	y;
	t_game		game;

	if (game_init(&game) != 0)
		return (1);
	while (1)
	{
		if (build_heat_map(&game) != 0)
			return (1);
		if (choose_placement(&game, &x, &y) >= 0)
			ft_dprintf(1, "%u %u\n", x, y);
		else
			break ;
		if (read_fields(&game))
			return (1);
	}
	write(1, "0 0\n", 4);
	wipe_out(&game);
	return (0);
}
