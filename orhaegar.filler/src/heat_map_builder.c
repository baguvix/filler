/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   heat_map_builder.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 18:32:28 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:47:39 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int	set_heat(t_field *h, unsigned ox, unsigned oy, int w)
{
	h->data[ox * h->y + oy] = w;
	return (w);
}

int			heat_up(t_qd *q, t_field *h, t_dot *d)
{
	int			rc;
	unsigned	i;
	unsigned	j;

	++d->w;
	i = d->x;
	j = d->y;
	rc = 0;
	if (i > 0)
		if (h->data[h->y * (i - 1) + j] == -3
			|| h->data[h->y * (i - 1) + j] > d->w)
			rc = enqueue(q, i - 1, j, set_heat(h, i - 1, j, d->w));
	if (!rc && j < h->y - 1)
		if (h->data[h->y * i + (j + 1)] == -3
			|| h->data[h->y * i + (j + 1)] > d->w)
			rc = enqueue(q, i, j + 1, set_heat(h, i, j + 1, d->w));
	if (!rc && i < h->x - 1)
		if (h->data[h->y * (i + 1) + j] == -3
			|| h->data[h->y * (i + 1) + j] > d->w)
			rc = enqueue(q, i + 1, j, set_heat(h, i + 1, j, d->w));
	if (!rc && j > 0)
		if (h->data[h->y * i + (j - 1)] == -3
			|| h->data[h->y * i + (j - 1)] > d->w)
			rc = enqueue(q, i, j - 1, set_heat(h, i, j - 1, d->w));
	return (rc);
}

t_qd		*gather_enemy_location(t_game *g)
{
	unsigned	i;
	unsigned	j;
	t_qd		*q;

	if ((q = create_queue(g->heat_map.x * g->heat_map.y)) == NULL)
		return (NULL);
	i = 0;
	while (i < g->plateau.x)
	{
		j = 0;
		while (j < g->plateau.y)
		{
			if (g->plateau.data[g->plateau.y * i + j] == '.')
				g->heat_map.data[g->heat_map.y * i + j] = -3;
			else if (g->plateau.data[g->plateau.y * i + j] == g->player
	|| ft_toupper(g->plateau.data[g->plateau.y * i + j]) == g->player)
				g->heat_map.data[g->heat_map.y * i + j] = -2;
			else if (enqueue(q, i, j,
					g->heat_map.data[g->heat_map.y * i + j] = -1))
				return (kill_queue(&q));
			++j;
		}
		++i;
	}
	return (q);
}

int			build_heat_map(t_game *game)
{
	t_dot		point;
	t_qd		*q;

	if ((q = gather_enemy_location(game)) == NULL)
		return (1);
	while (!dequeue(q, &point.x, &point.y, &point.w))
	{
		if (heat_up(q, &game->heat_map, &point))
		{
			kill_queue(&q);
			return (1);
		}
	}
	kill_queue(&q);
	return (0);
}
