/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_piece.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 18:34:56 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/22 00:01:37 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		do_check(t_game *g, unsigned ox, unsigned oy, int *score)
{
	if (ox >= g->plateau.x || oy >= g->plateau.y
		|| g->heat_map.data[(ox) * g->heat_map.y + oy] == -1)
		return (-1);
	if (g->heat_map.data[(ox) * g->heat_map.y + oy] == -2)
		return (1);
	else if (g->heat_map.data[(ox) * g->heat_map.y + oy] != -3)
		*score += g->heat_map.data[(ox) * g->heat_map.y + oy];
	else
		*score += (ox) * g->heat_map.y + oy;
	return (0);
}

int		check_match(t_game *g, unsigned root_x, unsigned root_y)
{
	unsigned	i;
	unsigned	j;
	int			score;
	int			self_collision;
	int			rc;

	score = 0;
	self_collision = 0;
	i = 0;
	while (i < g->piece.x)
	{
		j = 0;
		while (j < g->piece.y)
		{
			if (g->piece.data[i * g->piece.y + j] == '*')
			{
				if ((rc = do_check(g, root_x + i, root_y + j, &score)) == -1)
					return (-1);
				self_collision += rc;
			}
			++j;
		}
		++i;
	}
	return (self_collision == 1 ? score : -1);
}

int		choose_placement(t_game *g, unsigned *x, unsigned *y)
{
	unsigned	i;
	unsigned	j;
	int			score;
	int			best_score;

	best_score = -1;
	i = 0;
	while (i < g->heat_map.x)
	{
		j = 0;
		while (j < g->heat_map.y)
		{
			if ((score = check_match(g, i, j)) != -1)
				if (score < best_score || best_score == -1)
				{
					best_score = score;
					*x = i;
					*y = j;
				}
			++j;
		}
		++i;
	}
	return (best_score);
}
