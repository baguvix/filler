/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_managment.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 18:30:19 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:41:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char	get_player_symbol(void)
{
	char	*input;
	char	*player_ptr;
	char	player;
	int		rc;

	if ((rc = get_next_line(0, &input)) <= 0)
		return (rc == 0 ? 0 : eject("get_next_line", 0));
	if ((player_ptr = ft_strstr(input, "exec p")) == NULL)
		return (eject("undefined player[broken VM]", 0));
	player_ptr += ft_strlen("exec p");
	if (ft_atoi(player_ptr) == 1)
		player = 'O';
	else
		player = 'X';
	free(input);
	return (player);
}

int		get_dimensions(unsigned *x, unsigned *y)
{
	char	*input;
	char	*num;
	int		rc;

	if ((rc = get_next_line(0, &input)) <= 0)
		return (rc == 0 ? 0 : eject("get_next_line", 1));
	if ((num = ft_strchr(input, ' ')) == NULL)
		return (eject("undefined dimension[broken VM]", 1));
	*x = ft_atoi(++num);
	if ((num = ft_strchr(num, ' ')) == NULL)
		return (eject("undefined dimension[broken VM]", 1));
	*y = ft_atoi(num);
	free(input);
	return (0);
}

int		create_field(t_field *field)
{
	field->data = (int *)ft_memalloc(field->x * field->y * sizeof(int));
	return (field->data ? 0 : 1);
}

int		get_field(t_field *field, char op)
{
	char		*input;
	char		*str;
	unsigned	i;
	unsigned	j;

	if (op == 'm')
	{
		if ((i = get_next_line(0, &input)) <= 0)
			return (i == 0 ? 0 : eject("get_next_line", 1));
		free(input);
	}
	i = 0;
	while (i < field->x)
	{
		if ((j = get_next_line(0, &input)) <= 0)
			return (j == 0 ? 0 : eject("get_next_line", 1));
		str = op == 'm' ? ft_strchr(input, ' ') + 1 : input;
		j = -1;
		while (++j < field->y)
			field->data[field->y * i + j] = (int)str[j];
		free(input);
		++i;
	}
	return (0);
}
