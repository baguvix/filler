/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   runtime_managment.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/20 18:35:49 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:05:46 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		game_init(t_game *g)
{
	if ((g->player = get_player_symbol()) == 0)
		return (1);
	if (get_dimensions(&g->plateau.x, &g->plateau.y)
		|| create_field(&g->plateau)
		|| get_field(&g->plateau, 'm'))
		return (1);
	g->piece.x = g->plateau.x;
	g->piece.y = g->plateau.y;
	if (create_field(&g->piece)
		|| get_dimensions(&g->piece.x, &g->piece.y)
		|| get_field(&g->piece, 'p'))
		return (1);
	g->heat_map.x = g->plateau.x;
	g->heat_map.y = g->plateau.y;
	return (create_field(&g->heat_map));
}

void	wipe_out(t_game *g)
{
	free(g->plateau.data);
	free(g->heat_map.data);
	free(g->piece.data);
}

int		eject(char const *err, int status)
{
	perror(err);
	return (status);
}
