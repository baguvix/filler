/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpicktop.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:40:50 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:40:52 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_lstpicktop(t_list **top)
{
	void	*tmp;
	t_list	*n;

	if (*top == NULL)
		return (NULL);
	tmp = (*top)->content;
	n = (*top)->next;
	free(*top);
	*top = n;
	return (tmp);
}
