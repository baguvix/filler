/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:49:06 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:49:07 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strcmp(const char *s1, const char *s2)
{
	unsigned char	*uc_s1;
	unsigned char	*uc_s2;

	uc_s1 = (unsigned char*)(s1);
	uc_s2 = (unsigned char*)(s2);
	while ((*uc_s1 == *uc_s2) && *uc_s1)
	{
		++uc_s1;
		++uc_s2;
	}
	return (*uc_s1 - *uc_s2);
}
