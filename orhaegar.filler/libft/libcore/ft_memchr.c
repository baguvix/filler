/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:42:08 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:42:10 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char *c_s;
	unsigned char c_c;

	c_s = (unsigned char*)(s);
	c_c = (unsigned char)(c);
	while (n)
	{
		if (*c_s == c_c)
			return ((void*)(c_s));
		++c_s;
		--n;
	}
	return (NULL);
}
