/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:52:54 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:52:56 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	unsigned char	*uc_s1;
	unsigned char	*uc_s2;

	if (!n)
		return (0);
	uc_s1 = (unsigned char*)(s1);
	uc_s2 = (unsigned char*)(s2);
	while (--n && *uc_s1)
		if (*uc_s1++ != *uc_s2++)
			return (*(uc_s1 - 1) - *(uc_s2 - 1));
	return (*uc_s1 - *uc_s2);
}
