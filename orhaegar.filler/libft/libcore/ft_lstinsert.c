/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstinsert.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/12 17:56:44 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/09 13:24:14 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstinsert(t_list *after, t_list *new)
{
	t_list	*tmp;

	if (after == 0 || new == 0)
		return ;
	tmp = after->next;
	after->next = new;
	new->next = tmp;
}
