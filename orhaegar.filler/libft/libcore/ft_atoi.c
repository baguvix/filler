/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 19:31:54 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:31:21 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	short	sign;
	long	sum;

	while ((*str >= '\t' && *str <= '\r') || *str == ' ')
		++str;
	sign = 1;
	sum = 0;
	if (*str == '-')
		sign = -1;
	if (*str == '-' || *str == '+')
		++str;
	while ('0' <= *str && *str <= '9')
	{
		sum = (sum * 10) + (*str++ - '0');
		if (sum < 0)
			return (sign > 0 ? -1 : 0);
	}
	return (int)(sum * sign);
}
