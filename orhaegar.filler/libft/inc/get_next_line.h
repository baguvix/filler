/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/09 15:01:50 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/09 17:42:20 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/uio.h>

# define BUFF_SIZE 	1024

typedef	struct		s_file
{
	int				fd;
	struct s_file	*next;
	char			*lp;
	char			*reminder;
}					t_file;

int					get_next_line(const int fd, char **line);

#endif
