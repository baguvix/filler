/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 16:09:42 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/18 09:27:32 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

# include "ft_printf.h"
# include "libft.h"
# include <stdlib.h>

int		nofd(unsigned long long a, short base);
void	del_with_free(void *content, size_t size);
int		cmp(t_list *l1, t_list *l2);
void	del(void *lst, size_t content_size);
int		unitoa(char *dest, wchar_t c);

int		init_spec(t_spec *spec, size_t id);
void	set_sval(t_spec *spec, t_value *value, char sign);
t_list	*sort_specs(t_list *spec_list);

#endif
