/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_spec.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 02:03:36 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/12 19:30:20 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"

int		init_spec(t_spec *spec, size_t id)
{
	if (!spec)
		return (-1);
	ft_memset(spec, 0, sizeof(t_spec));
	spec->filling = ' ';
	spec->prec = -1;
	spec->id = id;
	return (0);
}

void	set_sval(t_spec *spec, t_value *value, char sign)
{
	spec->value = *value;
	if (sign)
		spec->sign = '-';
}

t_list	*sort_specs(t_list *sp_l)
{
	t_list	*head;
	t_list	*head_wz;

	head = 0;
	head_wz = 0;
	while (sp_l)
	{
		if (((t_spec *)(sp_l->content))->value.vp == 0 &&
			(((t_spec *)(sp_l->content))->num || !head))
			ft_lstpush(&head, ft_lstnewnocpy(sp_l->content, sizeof(t_spec)));
		if (((t_spec *)(sp_l->content))->value.vp == 0)
			ft_lstpush(&head_wz, ft_lstnewnocpy(sp_l->content, sizeof(t_spec)));
		sp_l = sp_l->next;
	}
	ft_lstsort(head, &cmp);
	sp_l = head_wz;
	while (sp_l && sp_l->next)
	{
		if (((t_spec *)sp_l->next->content)->num == 0)
			ft_lstinsert(ft_lstfind(head, sp_l->content, sizeof(t_spec)),
			ft_lstnewnocpy(sp_l->next->content, sizeof(t_spec)));
		sp_l = sp_l->next;
	}
	ft_lstdel(&head_wz, &del);
	return (head);
}
