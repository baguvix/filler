/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/27 10:39:25 by rnarbo            #+#    #+#             */
/*   Updated: 2020/09/21 22:45:18 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse_format.h"
#include "libft.h"
#include "utils.h"

#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>
#include <wchar.h>
#include <stdio.h>

static size_t	parse_str(t_spec *spec, char *format)
{
	char *p_percent;

	if ((p_percent = ft_strchr(format, '%')) != 0)
	{
		*p_percent = 0;
		spec->value.vp = ft_strdup(format);
		spec->type = '0';
		*p_percent = '%';
		return (p_percent - format);
	}
	spec->value.vp = ft_strdup(format);
	spec->type = '0';
	return (ft_strlen(spec->value.vp));
}

static size_t	parse_star(va_list ap, t_spec *spec)
{
	if (spec->width == -1)
		spec->width = va_arg(ap, int);
	if (spec->width < 0)
	{
		spec->width = -spec->width;
		spec->align_left = 1;
	}
	if (spec->prec == -2)
		spec->prec = va_arg(ap, int);
	if (spec->prec < 0)
		spec->prec = -1;
	return (1);
}

static ssize_t	parse_all(t_spec *spec, char *format)
{
	size_t i;

	i = 0;
	while (format[i])
		if (ft_isdigit(format[i]))
			i += parse_digit(spec, format + i);
		else if (format[i] == '*' && ++i)
			spec->width = -1;
		else if (format[i] == '.')
			i += parse_prec(spec, format + i + 1) + 1;
		else if (ft_strchr(LENGTHS, format[i]))
			i += parse_length(spec, format + i);
		else if (parse_flags(spec, format + i))
			i++;
		else if (format[i] == '{' || format[i] == '[')
			i += parse_color(spec, format + i);
		else
		{
			spec->type = format[i];
			return (++i);
		}
	spec->type = format[i];
	return (i + (format[i] == '\0' ? 0 : 1));
}

static void		parse_value(va_list ap, t_list *spec_head)
{
	t_list		*sspecs;
	t_list		*tmp;
	t_sgvalue	*vals;
	size_t		vget;

	sspecs = sort_specs(spec_head);
	tmp = sspecs;
	vget = 0;
	if ((vals = (t_sgvalue *)malloc((ft_lstlen(sspecs) + 1)
		* sizeof(t_sgvalue))) == 0)
		return ;
	while (tmp != 0)
	{
		if (parse_star(ap, tmp->content) && (((t_s)tmp->content)->num > vget
					|| ((t_spec *)tmp->content)->num == 0))
			vget += take_value(tmp->content, ap, vals + vget);
		if (((t_spec *)tmp->content)->num == 0)
			set_sval(tmp->content, &vals[vget - 1].val, vals[vget - 1].sign);
		else
			set_sval(tmp->content,
			&vals[((t_spec *)tmp->content)->num - 1].val, vals[vget - 1].sign);
		tmp = tmp->next;
	}
	ft_lstdel(&sspecs, &del);
	free(vals);
}

size_t			parse_format(va_list ap, t_list **spec_head, const char *frm)
{
	char	*format;
	int		i;
	int		id;
	t_spec	tmp_spec;

	if (!spec_head || !frm)
		return (0);
	if ((format = ft_strdup(frm)) == 0)
		return (0);
	i = 0;
	id = 0;
	while (format[i])
	{
		init_spec(&tmp_spec, id++);
		if (format[i] == '%' && ++i)
			i += parse_all(&tmp_spec, format + i);
		else
			i += parse_str(&tmp_spec, format + i);
		ft_lstpush(spec_head, ft_lstnew(&tmp_spec, sizeof(t_spec)));
	}
	parse_value(ap, *spec_head);
	free(format);
	return (id);
}
