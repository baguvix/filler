/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fp_core.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/15 20:55:11 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/21 22:43:33 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "fp_core.h"

static size_t	bp_count(t_bn *a)
{
	size_t		tmp;
	long double	t;

	tmp = 0;
	t = a->a.f;
	if (t >= 0.0 && t <= 1.0)
		return (1);
	while (t >= 1.0)
	{
		++tmp;
		t /= 10;
	}
	return (tmp);
}

int				gen_num_neg(t_bn *n, t_spec *sanya, char **s)
{
	size_t	tmp;
	size_t	bp;

	n->len = sanya->prec / 18 + 3;
	n->p = (t_ull *)ft_memalloc(sizeof(t_ull) * n->len);
	bp = bp_count(n);
	tmp = n->len * 18 - (nofd((unsigned long long)(n->a.df.low), 10) - bp) + 1;
	*s = (char *)malloc(tmp + 1);
	if (n->p == NULL || *s == NULL)
		return (1);
	n->p[0] = n->a.df.low;
	while (n->p[0] > DIGIT_MASK)
	{
		n->p[1] = (n->p[1] / 10) + (n->p[0] % 10) * DIGIT_MASK / 10;
		n->p[0] /= 10;
	}
	while (n->pow++)
		div_2(n);
	ft_memset(*s, '0', tmp);
	num_map(n, *s + 1, tmp - 1);
	correction(*s, bp + sanya->prec + 1);
	ft_memmove(*s, *s + 1, bp);
	(*s)[bp] = (sanya->prec > 0) ? '.' : '\0';
	(*s)[bp + sanya->prec + 1] = '\0';
	return (0);
}

int				gen_num_nneg(t_bn *n, t_spec *sanya, char **s)
{
	size_t	i;

	n->len = DEFAULT_SIZE;
	n->p = (t_ull *)ft_memalloc(sizeof(t_ull) * n->len);
	if (n->p == NULL)
		return (1);
	n->p[n->len - 1] = n->a.df.low % DIGIT_MASK;
	n->p[n->len - 2] = (n->a.df.low - n->p[n->len - 1]) / DIGIT_MASK;
	while (n->pow--)
		mul_2(n);
	i = 0;
	while (n->p[i] == 0)
		++i;
	i = nofd(n->p[i], 10) + sanya->prec + (n->len - i - 1) * 18 + 1;
	*s = (char *)malloc(i + 1);
	if (*s == NULL)
		return (1);
	ft_memset(*s, '0', i);
	(*s)[i] = '\0';
	i -= (sanya->prec + 1);
	(*s)[i] = '.';
	num_map(n, *s, i);
	return (0);
}

ssize_t			wrap_up(t_spec *s, char **rtu)
{
	size_t	size;
	size_t	len;
	char	*stmp;

	len = ft_strlen(s->value.vp);
	if (s->sign)
		++len;
	size = ((size_t)s->width > len ? s->width : len);
	if (s->align_left)
		s->filling = ' ';
	if ((*rtu = (char *)malloc(size + 1)) == NULL)
		return (-1);
	ft_memset(stmp = *rtu, s->filling, size);
	if (s->sign && s->filling == '0')
		*stmp++ = s->sign;
	stmp += (s->align_left ? 0 : size - len);
	if (s->sign && s->filling != '0')
		*stmp++ = s->sign;
	ft_strncpy(stmp, s->value.vp, ft_strlen(s->value.vp));
	if (s->type == 'F' || s->type == 'E')
		ft_capitalize(*rtu, size);
	free(s->value.vp);
	return (size);
}

ssize_t			naninf(t_bn *n, t_spec *sanya, char **rtu)
{
	int		high_bits;

	if ((*rtu = (char *)malloc(4 * sizeof(char))) == NULL)
		return (-1);
	high_bits = n->a.df.low >> 62;
	sanya->sign = 0;
	sanya->filling = ' ';
	ft_strcpy(*rtu, "nan");
	if (high_bits < 3)
		if (!(n->a.df.low << 2))
		{
			sanya->sign = n->sign;
			ft_strcpy(*rtu, "inf");
		}
	return (0);
}
