/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oxp_conversions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/03 19:23:23 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/21 21:43:54 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "utils.h"
#include "libft.h"

static char		*descend(unsigned long long a, short base, char *str, int prec)
{
	char tmp;

	while (prec-- > 0)
	{
		if ((tmp = a % base) > 9)
			*str = tmp - 10 + 'a';
		else
			*str = tmp + '0';
		--str;
		a /= base;
	}
	return (str);
}

ssize_t			o_conv(t_spec *s, char **rtu)
{
	size_t	len;
	size_t	tmp;
	char	*stmp;

	len = (size_t)nofd(s->value.ll, 8);
	if (s->prec != -1)
		s->filling = ' ';
	tmp = (s->prec < (int)len ? len : s->prec);
	if (s->alt && s->value.ll != 0 && ((int)len >= s->prec || s->prec == -1))
		tmp += 1;
	len = (s->width < (int)tmp ? tmp : s->width);
	if ((*rtu = (char *)malloc(len * sizeof(char))) == NULL)
		return (-1);
	ft_memset(stmp = *rtu, ' ', len);
	stmp += (s->align_left ? tmp - 1 : len - 1);
	if (s->filling == '0')
		tmp = stmp - *rtu + 1;
	if (s->prec == 0 && s->value.ll == 0 && !s->alt)
		tmp = 0;
	stmp = descend(s->value.ll, 8, stmp, tmp);
	if (s->alt && s->value.ll != 0 && ((int)len >= s->prec || s->prec == -1))
		stmp[1] = '0';
	len = (tmp == 0 && s->width == 0 ? 0 : len);
	return (len);
}

ssize_t			x_conv(t_spec *s, char **rtu)
{
	size_t	len;
	size_t	tmp;
	char	*stmp;

	len = (size_t)nofd(s->value.ll, 16);
	if (s->prec != -1)
		s->filling = ' ';
	tmp = (s->prec < (int)len ? len : s->prec);
	tmp += (s->alt && s->value.ll != 0 ? 2 : 0);
	len = (s->width < (int)tmp ? tmp : s->width);
	if ((*rtu = (char *)malloc(len * sizeof(char))) == NULL)
		return (-1);
	ft_memset(stmp = *rtu, ' ', len);
	stmp += (s->align_left ? tmp - 1 : len - 1);
	tmp = (s->filling == '0' ? stmp - *rtu + 1 : tmp);
	if (s->prec == 0 && s->value.ll == 0)
		tmp = 0;
	stmp = descend(s->value.ll, 16, stmp, tmp);
	if (s->alt && s->value.ll != 0 && (stmp[1] = '0'))
		stmp[2] = 'x';
	len = (tmp == 0 && s->width == 0 ? 0 : len);
	if (s->type == 'X')
		ft_capitalize(*rtu, len);
	return (len);
}

ssize_t			p_conv(t_spec *s, char **rtu)
{
	size_t	len;
	size_t	width;
	char	*sp;

	len = (size_t)nofd(s->value.ll, 16);
	if (s->value.ll == 0 && s->prec == 0)
		len--;
	len = (s->prec > (int)len) ? s->prec : len;
	len += 2;
	width = (int)len > s->width ? len : s->width;
	if (s->prec == -1 && s->filling == '0')
		len = width;
	if ((*rtu = (char *)malloc(width)) == 0)
		return (-1);
	ft_memset(*rtu, s->filling, width);
	sp = s->align_left ? *rtu : *rtu + width - len;
	sp = descend(s->value.ll, 16, sp + len - 1, len);
	sp[2] = 'x';
	sp[1] = '0';
	return (width);
}
