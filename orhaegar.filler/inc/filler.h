/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/09 17:35:28 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:37:40 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include <stdio.h>
# include "libft.h"
# include "get_next_line.h"
# include "hard_queue.h"

typedef	struct		s_field
{
	int				*data;
	unsigned int	x;
	unsigned int	y;
}					t_field;

typedef struct		s_game
{
	t_field			plateau;
	t_field			heat_map;
	t_field			piece;
	char			player;
}					t_game;

int					build_heat_map(t_game *game);
int					game_init(t_game *g);
void				wipe_out(t_game *g);
int					choose_placement(t_game *g, unsigned *x, unsigned *y);
int					get_dimensions(unsigned *x, unsigned *y);
int					get_field(t_field *field, char op);
char				get_player_symbol(void);
int					create_field(t_field *field);
int					eject(char const *err, int status);
#endif
