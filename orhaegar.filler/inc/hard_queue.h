/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hard_queue.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/14 15:33:29 by orhaegar          #+#    #+#             */
/*   Updated: 2020/09/21 23:46:44 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HARD_QUEUE_H
# define HARD_QUEUE_H

typedef	struct	s_dot
{
	unsigned	x;
	unsigned	y;
	int			w;
}				t_dot;

typedef	struct	s_queue
{
	size_t		status;
	t_dot		dot;
}				t_queue;

typedef	struct	s_qd
{
	size_t		size;
	size_t		curr;
	size_t		end;
	t_queue		*queue;
}				t_qd;

t_qd			*create_queue(size_t size);
int				enqueue(t_qd *q, unsigned x, unsigned y, int w);
int				dequeue(t_qd *q, unsigned *x, unsigned *y, int *w);
void			*kill_queue(t_qd **q);

#endif
